package com.hendisantika.springbootthymeleaf.controller;

import com.hendisantika.springbootthymeleaf.entity.Person;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/12/19
 * Time: 20.13
 */
@Controller
public class PersonController {

    @GetMapping("/persons")
    public ModelAndView getAllPersons() {

        ModelAndView modelAndView = new ModelAndView("persons");
        modelAndView.addObject("persons", Arrays.asList(new Person("John", "Doe", 24.1, "M"), new Person("Uchiha", "Sasuke", 22.3, "F")));
        return modelAndView;
    }
}