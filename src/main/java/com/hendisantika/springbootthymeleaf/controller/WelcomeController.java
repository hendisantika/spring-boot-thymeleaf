package com.hendisantika.springbootthymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/12/19
 * Time: 20.10
 */
@Controller
public class WelcomeController {

    @GetMapping({"/", "/welcome"})
    public String hello(Model model) {
        System.out.println("Hello");
        model.addAttribute("date", new Date());
        return "welcome";
    }
}