package com.hendisantika.springbootthymeleaf.entity;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/12/19
 * Time: 20.09
 */
public class Notification {
    private String notificationId;
    private String notificationMessage;

    public Notification() {
    }

    public Notification(String notificationId, String notificationMessage) {
        this.notificationId = notificationId;
        this.notificationMessage = notificationMessage;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "notificationId='" + notificationId + '\'' +
                ", notificationMessage='" + notificationMessage + '\'' +
                '}';
    }
}
